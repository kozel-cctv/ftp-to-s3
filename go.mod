module gitlab.com/kozel-cctv/ftp-to-s3

go 1.19

require (
	github.com/aws/aws-lambda-go v1.36.0
	github.com/aws/aws-sdk-go v1.44.153
	github.com/jlaffaye/ftp v0.1.0
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
