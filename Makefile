.PHONY: build clean deploy gomodgen

build:
	export GO111MODULE=on
	env GOOS=linux env GOARCH=amd64 env CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/kozel-photos-sync main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock

deploy: clean build
	serverless deploy --verbose
