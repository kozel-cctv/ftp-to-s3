package main

import (
	"bytes"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/jlaffaye/ftp"
	"io"
	"log"
	"os"
	"time"
)

var sess = session.Must(session.NewSession(&aws.Config{
	Region: aws.String(endpoints.EuWest1RegionID),
}))

var uploader = s3manager.NewUploader(sess)

func main() {
	lambda.Start(doSync)
}

func doSync() {
	ftpHost := os.Getenv("FTP_HOST")
	ftpUser := os.Getenv("FTP_USER")
	ftpPass := os.Getenv("FTP_PASS")

	c, err := ftp.Dial(ftpHost, ftp.DialWithTimeout(5*time.Second), ftp.DialWithDisabledEPSV(true))
	handleError(err)

	err = c.Login(ftpUser, ftpPass)
	handleError(err)

	accounts := []string{"kozel", "lojza"}

	for _, account := range accounts {
		dirs, err := c.List(account + "/")
		handleError(err)

		if len(dirs) == 0 {
			fmt.Println("No files to sync")
		}

		for _, dir := range dirs {
			if dir.Type != ftp.EntryTypeFolder || dir.Name == "." || dir.Name == ".." {
				continue
			}

			// list all images in folder
			images, err := c.List(account + "/" + dir.Name + "/images/")
			handleError(err)

			imagesCount := len(images)
			iterator := 0
			isEmpty := true // flag to check if folder is empty ('.' and '..' files)
			for _, image := range images {
				iterator++

				if image.Type != ftp.EntryTypeFile {
					continue
				}

				isEmpty = false

				filePath := account + "/" + dir.Name + "/images/" + image.Name
				fileResource, err := c.Retr(filePath)
				handleError(err)

				b, err := io.ReadAll(fileResource)
				handleError(err)

				key := account + "/" + dir.Name + "/" + image.Name
				err = uploadFile(key, bytes.NewReader(b))
				handleError(err)

				// the newest image
				if imagesCount == iterator {
					key := account + "/the-newest.jpg"
					err = uploadFile(key, bytes.NewReader(b))
				}

				// cleanup
				err = fileResource.Close()
				handleError(err)
				err = c.Delete(filePath)
				handleError(err)
			}

			// empty folder = delete
			if isEmpty {
				err = c.RemoveDirRecur(account + "/" + dir.Name)
				handleError(err)
			}

		}
	}

	err = c.Quit()
	handleError(err)
}

// Upload the file to S3.
func uploadFile(key string, content io.Reader) error {
	s3Bucket := os.Getenv("AWS_S3_BUCKET")
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s3Bucket),
		Key:    aws.String(key),
		Body:   content,
	})
	handleError(err)
	fmt.Println(result.Location)

	return nil
}

func handleError(err error) {
	if err != nil {
		l := log.New(os.Stderr, "", 0)
		l.Println(err.Error())
		panic(err)
	}
}
