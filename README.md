# Sync between FTP and S3

1. CCTV stores images on FTP server. Format: `kozel/<dir_with_date>/images/<image_with_datetime>`
2. This program runs every minute and sync all images from FTP to AWS s3 storage. It also deletes successfully synced
   images from FTP.
3. This program is written in GoLang and it uses Go modules
4. This programs is deployed in AWS Lambda using Serverless framework. See `serverless.yml`.

## Deploy

```
cp .env.dist .env
cp conf.yml.dist conf.yml
# fill all variables
./loadEnvs.sh
make deploy # build and deploy
```